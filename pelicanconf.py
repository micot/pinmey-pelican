#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from datetime import datetime

AUTHOR = 'pinmey'
SITEDESCRIPTION = "Kevin and Mega's personal website"
SITENAME = 'Pinmey'
SITESUBTITLE = "Kevin and Mega's personal website"
SITETITLE = 'Pinmey'
# SITEURL = 'http://www.pinmey.com'
SITEURL = 'http://localhost:8000'

PATH = 'content'
TIMEZONE = 'Asia/Jakarta'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('blog', '/blog'),
         ('categories', '/categories'),
         ('guestbook', '#'))

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

## THEME related
THEME = 'themes/Flex'
USE_LESS = True
COPYRIGHT_YEAR = datetime.now().year
DEFAULT_PAGINATION = 10
MAIN_MENU = True
MENUITEMS = (('Archives', '/archives.html'),
			 ('Categories', '/categories.html'),
			 ('Tags', '/tags.html'),)

# SOCIAL = (('github', 'https://instagram.com/megasariperdana'))

DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = True

## CONTENT related
USE_FOLDER_AS_CATEGORY = True
DEFAULT_CATEGORY = 'misc'
STATIC_PATHS = ['images', 'references']
PAGE_EXCLUDES = ['references']
ARTICLE_EXCLUDES = ['references']

## URLs
#RELATIVE_URLS = True
ARTICLE_URL = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
#ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/{date:%d}/{slug}.html'
PAGE_URL = '{category}/{slug}/'
PAGE_SAVE_AS = '{category}/{slug}/index.html'

## PLUGINS
PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['neighbors'] #https://github.com/getpelican/pelican-plugins/tree/master/neighbors
